local AceOO = AceLibrary("AceOO-2.0")
local SpellStatus = AceLibrary("SpellStatus-1.0")
local SpellCache = AceLibrary("SpellCache-1.0")

local ECastBar = AceOO.Class(IceBarElement)
require Lists
ECastBar.Actions = {}
ECastBar.prototype.action = nil
ECastBar.prototype.actionStartTime = nil
ECastBar.prototype.actionMessage = nil

-- Constructor --
function ECastBar.prototype:init()
	ECastBar.super.prototype.init(self, "ECastBar")
	self:SetDefaultColor("CastCasting", 242, 242, 10)
	self:SetDefaultColor("CastChanneling", 117, 113, 161)
	self:SetDefaultColor("CastSuccess", 242, 242, 70)
	self:SetDefaultColor("CastFail", 1, 0, 0)
	self.delay = 0
	--self.action = ECastBar.Actions.None
end


function ECastBar.prototype:Enable(core)
	ECastBar.super.prototype.Enable(self, core)

	self:RegisterEvent("CHAT_MSG_SPELL_HOSTILEPLAYER_DAMAGE")
	self:RegisterEvent("CHAT_MSG_SPELL_SELF_DAMAGE")
	self:RegisterEvent("CHAT_MSG_SPELL_HOSTILEPLAYER_BUFF")
	self:RegisterEvent("CHAT_MSG_SPELL_FRIENDLYPLAYER_DAMAGE")
	self:RegisterEvent("CHAT_MSG_SPELL_FRIENDLYPLAYER_BUFF")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_HOSTILEPLAYER_BUFFS")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_FRIENDLYPLAYER_BUFFS")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_HOSTILEPLAYER_DAMAGE")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_FRIENDLYPLAYER_DAMAGE")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_SELF_DAMAGE")
	self:RegisterEvent("CHAT_MSG_SPELL_PARTY_DAMAGE")
	self:RegisterEvent("CHAT_MSG_SPELL_PARTY_BUFF")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_PARTY_DAMAGE")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_PARTY_BUFFS")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_CREATURE_DAMAGE")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_CREATURE_BUFFS")
	self:RegisterEvent("CHAT_MSG_SPELL_CREATURE_VS_CREATURE_DAMAGE")

	self.frame:Hide()

	-- remove blizz cast bar
	CastingBarFrame:UnregisterAllEvents()
end
